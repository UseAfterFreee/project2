#include "buck.h"

// Constructor
Buck::Buck(int voltPin, int outputPin)
    : outputPin(outputPin), sensors(voltPin), output(127)
{

}

void Buck::writePWM()
{
    // Lower limit
    if (output < 20) output = 20;
    analogWrite(outputPin, output);
    // Wait for everything to update
    delay(500);
}

void Buck::setVoltage()
{
    // Buck to the correct voltage
    writePWM();
    sensors.read();
    //Serial.println(sensors.voltageReading, 4);
    if (sensors.voltageReading < requiredVoltage)
        output += 1;
    else if (sensors.voltageReading > requiredVoltage)
        output -= 1;
    return;
}

void Buck::track()
{
    // Only do something if its currently not in a delay
    setVoltage();
    writePWM();
}
