#ifndef BUCK_H
#define BUCK_H

#include "Arduino.h"

#include "voltSensor.h"

#include "AsyncDelay.h"

class Buck
{
    private:
        int outputPin;

        VoltSensor sensors;

        int output; //0-255

        AsyncDelay delay500;

        float requiredVoltage = 23;
        void setVoltage();
        void writePWM();
    public:
        Buck(int voltPin, int outputPin);
        void track();
};

#endif
