#include "powerSensor.h"

PowerSensor::PowerSensor(int voltPin, int ampsPin)
    : VoltSensor(voltPin), ampsPin(ampsPin)
{
}

float PowerSensor::oneAmpsReading()
{
        // Read the amps pin
        unsigned long AmpsReadOut = analogRead(ampsPin);
        float mVolts = AmpsReadOut*5000/1024;
        float mVoltsDif = mVolts-2501.9;
        float Amps = (abs(mVoltsDif)/185) * 1.19;
        return Amps; 
}

void PowerSensor::read()
{
    float AverageAmps = 0;
    float AverageVolts = 0;
    for (int i = 0; i < samples; i++)
    {
        AverageAmps += oneAmpsReading();
        AverageVolts += oneVoltReading();
    }
    amperageReading = AverageAmps/samples;
    AverageVolts /= samples;
    voltageReading = AverageVolts/(1000*scaleFactor); // 1 MOhm and 51kOhm
    powerReading = abs(amperageReading) * abs(voltageReading);
}
