#ifndef POWERSENSOR_H
#define POWERSENSOR_H

#include "voltSensor.h"

class PowerSensor : public VoltSensor
{
    protected:
        int ampsPin;
        float oneAmpsReading();

    public:
        PowerSensor(int voltPin, int ampsPin);

        float amperageReading;
        float powerReading;
        void read();
};

#endif
