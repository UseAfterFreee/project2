#include "voltSensor.h"

VoltSensor::VoltSensor(int voltPin)
    : voltPin(voltPin)
{
}

float VoltSensor::oneVoltReading()
{
    // Read the voltage pin
    unsigned long VoltReadOut = analogRead(voltPin);
    float mVolts = (VoltReadOut*5000/1024)*1.014;
    return mVolts;
}

void VoltSensor::read()
{
    float AverageVolts = 0;

    for (int i = 0; i < samples; i++)
    {
        AverageVolts += oneVoltReading();
    }
    // Save the averages
    AverageVolts /= samples;
    voltageReading = AverageVolts/(1000*scaleFactor); // 1 MOhm and 51kOhm
}
