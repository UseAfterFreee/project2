#ifndef VOLTSENSOR_H
#define VOLTSENSOR_H

#include "Arduino.h"

class VoltSensor
{
    protected:
        float scaleFactor = 0.0586070208;
        const unsigned int samples = 100;
        int voltPin;
        float oneVoltReading();

    public:
        VoltSensor(int voltPin);

        float voltageReading;
        void read();
};

#endif
