#ifndef GRIDREADER_H
#define GRIDREADER_H

#include "Arduino.h"

enum class State
{
    S_LOW,
    S_HIGH
};

struct Pins
{
    int grid;
    int pwm1;
    int pwm2;
};

enum class DrawMode
{
    GRID,
    ANGULAR_FREQUENCY,
    FREQUENCY,
    NOTHING
};

class GridReader
{
    private:
        unsigned int lowerBound = 500;
        unsigned int higherBound = 900;

        State currentState = State::S_LOW;
        State previousState = State::S_LOW;

        unsigned long peakTime1 = 0;
        unsigned long peakTime2 = 0;

        unsigned long period = 20000;

        Pins pins;

        DrawMode mode = DrawMode::NOTHING;

    public:
        GridReader(int analogInputPin, int pwmOutputPin1, int pwmOutputPin2);

        void SetBounds(unsigned int lower, unsigned int higher);
        void SetDrawMode(DrawMode newMode);
        void ReadGrid();
};

#endif
