#include "gridreader.h"

GridReader::GridReader(int analogInputPin, int pwmOutputPin1, int pwmOutputPin2)
    : pins({analogInputPin, pwmOutputPin1, pwmOutputPin2})
{
    pinMode(pins.grid,INPUT);  
    pinMode(pins.pwm1, OUTPUT);
    pinMode(pins.pwm2, OUTPUT);
}

void GridReader::SetDrawMode(DrawMode newMode)
{
    mode = newMode;
}

void GridReader::ReadGrid()
{
    unsigned long timeNow = micros();
    int inputVal = analogRead(pins.grid);

    previousState = currentState;
    switch (currentState)
    {
        case State::S_LOW:
            if (inputVal > higherBound)
                currentState = State::S_HIGH;
            break;
        case State::S_HIGH:
            if (inputVal < lowerBound)
                currentState = State::S_LOW;
            break;
    }

    if (currentState == State::S_HIGH && previousState == State::S_LOW)
    {
        peakTime2 = timeNow;
        float newPeriod = peakTime2 - peakTime1;
        period += (newPeriod - period) * 0.02;
        peakTime1 = peakTime2;
    }

    float angularFrequency = 2 * 3.141592 * (1000000/(float)period);
    float timeOffset = 5100 - 1500;
    float inputTime = ((timeNow-peakTime1 + timeOffset)/(float)1000000);

    float toWrite = 127 * sin(angularFrequency * inputTime);
    toWrite = (toWrite > 125 ? 127 : toWrite);
    toWrite = (toWrite < -125 ? -127 : toWrite);
    float negToWrite = -toWrite + 127;
    toWrite = toWrite + 127;
    toWrite = (toWrite > 253 ? 255 : toWrite);
    toWrite = (toWrite < 5 ? 0 : toWrite);
    negToWrite = (negToWrite > 253 ? 255 : negToWrite);
    negToWrite = (negToWrite < 5 ? 0 : negToWrite);
    analogWrite(pins.pwm1, toWrite);
    analogWrite(pins.pwm2, negToWrite);
}

void GridReader::SetBounds(unsigned int lower, unsigned int higher)
{
    lowerBound = lower;
    higherBound = higher;
}
