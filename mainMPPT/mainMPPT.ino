#include <mppt.h>

/*
 * A1 Volt reading MPPT
 * A2 Amp reading MPPT
 * A3 Volt reading Buck
 * 5 MPPT Output
 * 6 Buck Output
 */
MPPT mppt(A1, A2, 5); // Create the object that controls the boost

void setup() {
    // Set the PWM Freqency of pin 5, 6, 3, 11
    // https://playground.arduino.cc/Main/TimerPWMCheatsheet
    TCCR0B = (TCCR0B & 0b11111000) | 0x02; // this messes up the millis micros and delay functions, carefull when using those
    //Serial.begin(9600);
}

void loop() {
    // Boost and Buck to the correct voltage
    mppt.track();
}
