#include <gridreader.h>

// A0 is the pin to which the optocoupler outputs and 5 is the pin to which we write the PWM sine wave
GridReader grid(A0, 11, 3);

void setup() {
    TCCR2B = (TCCR2B & 0b11111000) | 0x01;

    // Start the serial connection for drawing some input data
    //Serial.begin(2000000);

    // Set the draw mode to NOTHING that is calculated
    //grid.SetDrawMode(DrawMode::NOTHING);
    grid.SetBounds(200, 900); // Set the lower and upper bounds of the optocoupler
}

void loop() {
    // Read the grid and write the PWM to pins 3 and 11
    grid.ReadGrid();
}
