#ifndef MPPT_H
#define MPPT_H

#include "Arduino.h"

#include "powerSensor.h"

#include "AsyncDelay.h"

class MPPT
{
    private:
        int outputPin;

        PowerSensor sensors;

        int output; //0-255

        void writePWM();
        void maximumPower();
    public:
        MPPT(int voltsPin, int ampsPin, int outputPin);
        void track();
};

#endif
