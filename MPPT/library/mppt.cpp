#include "mppt.h"

void MPPT::writePWM()
{
    // Upper limit on boost
    if (output > 220) output = 220;
    if (output < 1) output = 1;

    if (sensors.voltageReading >= 70) output = 0; // Kill switch
    //output = 30;

    // Write the new pwm
    analogWrite(outputPin, output);
    // Wait a little to update everything
    delay(8000);
}

// Constructor
MPPT::MPPT(int voltsPin, int ampsPin, int outputPin)
    : outputPin(outputPin), sensors(voltsPin, ampsPin), output(30)
{

}

// Boost to maximum power
void MPPT::maximumPower() {
    writePWM();
    sensors.read();
    //Serial.println(sensors.voltageReading, 4);


    if (sensors.voltageReading < 36)
    {
        float oldVolt = sensors.voltageReading;
        output += 3;
        writePWM();
        sensors.read();
        if (sensors.voltageReading > oldVolt)
            return;
        if (sensors.voltageReading < oldVolt)
        {
            if (output <= 5) output = 7;
            output -= (2 * 3);
            writePWM();
            sensors.read();
            if (sensors.voltageReading > oldVolt)
                return;
            if (sensors.voltageReading < oldVolt)
            {
                output += 3;
                writePWM();
                sensors.read();
                return;
            }
            return;
        }
    }
    else
    {
        float oldVolt = sensors.voltageReading;
        output += 3;
        writePWM();
        sensors.read();
        if (sensors.voltageReading < oldVolt)
            return;
        if (sensors.voltageReading > oldVolt)
        {
            if (output <= 5) output = 7;
            output -= (2 * 3);
            writePWM();
            sensors.read();
            if (sensors.voltageReading > oldVolt)
            {
                output += 3;
                writePWM();
                return;
            }
            return;
        }
    }
}

void MPPT::track()
{
    // Only do something if there is no delay happening
    maximumPower();
}
